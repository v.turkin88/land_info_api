FactoryBot.define do
  factory :locale do
    abc = ('a'..'z').to_a.join

    active { false }
    sequence(:title) { |n| "locale#{n}" }
    sequence(:iso2) { |n| "a#{abc[n]}" }
    sequence(:iso3) { |n| "ab#{abc[n]}" }
  end
end
