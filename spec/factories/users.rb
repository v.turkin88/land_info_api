FactoryBot.define do
  factory :user do
    abc = ('a'..'z').to_a.join
    sequence(:email) { |n| "test_#{n}@gmail.com" }
    sequence(:first_name) { |n| "user#{abc[n]}" }
    sequence(:last_name) { |n| "user#{abc[n]}" }
    sequence(:verified) { true }
    sequence(:verified_at) { Time.now }

    password { 'secret!' }
    # password_confirmation { 'secret!' }

    trait :member do
      role { Role.get_member }
      # role {Role.find_by(name: 'member')}
    end

    trait :manager do
      role { Role.get_manager }
    end

    trait :admin do
      role { Role.get_admin }
    end
  end
end
