# frozen_string_literal: true

require 'simplecov'
SimpleCov.start :rails do
  add_group 'Models', 'app/models'
  add_group 'Controllers', 'app/controllers'
  add_group 'Lib', 'lib/'
  add_group 'Helpers', 'app/helpers'

  add_filter 'app/models/application_record.rb'
  add_filter 'app/channels/application_cable/channel.rb'
  add_filter 'app/channels/application_cable/connection.rb'
  add_filter 'app/jobs/application_job.rb'
  add_filter 'app/models/ability.rb'
  add_filter 'app/controllers/api/base_controller.rb'
  add_filter 'app/controllers/admin/base_controller.rb'
  add_filter 'app/controllers/application_controller.rb'
end

require 'webmock/rspec'
WebMock.disable_net_connect!(allow_localhost: true)

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups
end
