# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::Api::V1::FaqsController, type: :controller do
  render_views

  let(:response_body) { JSON.parse(response.body) }
  let(:member) { create :user, :member }
  let(:locale) { create :locale }

  let(:faq_params) do
    {
      title: 'Test1',
      description: 'Test Description2',
      locale_id: locale.id,
      active: true
    }
  end
  let(:faq_params1) do
    {
      title: 'Test2',
      description: 'Test Description2',
      locale_id: locale.id,
      active: true
    }
  end

  describe 'with success' do
    it 'for index' do
      Faq.create faq_params

      sign_in user: member

      get :index, params: { title: 'Test1', description: 'Test Description2', locale: locale.iso3 }

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['faqs'].count).to eq(1)
      expect(JSON.parse(response.body)['count']).to eq(1)
    end

    it 'for index with pagination' do
      Faq.create faq_params
      Faq.create faq_params1

      sign_in user: member

      get :index, params: { page: 1, per_page: 1, locale: locale.iso3 }

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['faqs'].count).to eq(1)
      expect(JSON.parse(response.body)['count']).to eq(2)
    end
  end
end
