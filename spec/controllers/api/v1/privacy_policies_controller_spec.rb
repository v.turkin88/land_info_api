require 'rails_helper'

RSpec.describe ::Api::V1::PrivacyPoliciesController, type: :controller do
  render_views

  let(:member) { create :user, :member }
  let(:response_body) { JSON.parse(response.body) }
  let(:locale) { create :locale, active: true }
  let(:valid_params) do
    {
      body: 'Test body',
      document_url: 'url',
      locale_id: locale.id,
    }
  end

  describe 'with success' do
    it 'for index when authorized user' do
      sign_in user: member

      PrivacyPolicy.first_or_create valid_params

      get :index, params: { locale: locale.iso3 }
      expect(response.status).to be 200
      expect(JSON.parse(response.body)['privacy_policy']['body']).to eq('Test body')
    end

    it 'for index when not authorized user' do
      PrivacyPolicy.first_or_create valid_params

      get :index, params: { locale: locale.iso3 }
      expect(response.status).to be 200
      expect(JSON.parse(response.body)['privacy_policy']['body']).to eq('Test body')
    end
  end

  describe 'with error' do
    it 'for index when authorized user' do
      sign_in user: member

      get :index
      expect(response.status).to be 422
      expect(JSON.parse(response.body)['message']).to eq('Locale not found')
    end

    it 'for index when not authorized user' do
      get :index
      expect(response.status).to be 422
      expect(JSON.parse(response.body)['message']).to eq('Locale not found')
    end
  end
end
