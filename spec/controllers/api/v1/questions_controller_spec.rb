# frozen_string_literal: true

require 'rails_helper'
require 'benchmark'

RSpec.describe ::Api::V1::QuestionsController, type: :controller do
  render_views

  let(:response_body) { JSON.parse(response.body) }
  let(:member) { create :user, :member }
  let(:manager) { create :user, :manager }

  let(:question_params) do
    {
      title: 'Test',
      description: 'Test Description',
      user_id: member.id,
      status: 'draft',
    }
  end

  describe 'with success' do
    it 'for index' do
      sign_in user: member

      get :index

      expect(response.status).to be 200
    end

    it 'for create' do
      sign_in user: member

      post :create, params: question_params

      expect(response.status).to be 200
    end

    it 'for destroy' do
      question = Question.create question_params

      sign_in user: member

      delete :destroy, params: { id: question.id }

      expect(response.status).to be 200
    end

    it 'for update' do
      question = Question.create question_params

      sign_in user: member

      put :update, params: { id: question.id, title: 'hhh' }

      expect(response.status).to be 200
      expect(Question.find(question.id).title).to eq('hhh')
    end
  end

  describe 'with error' do
    it 'for create news' do
      sign_in user: manager

      post :create, params: question_params

      expect(response.status).to be 403
    end
  end
end
