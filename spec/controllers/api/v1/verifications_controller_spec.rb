# frozen_string_literal: true

require 'rails_helper'
require 'benchmark'

RSpec.describe Api::V1::VerificationsController, type: :controller do
  render_views

  describe 'with success' do
    it 'for create user' do
      user = create :user, :member

      get :show, params: { id: user.verification_code }

      expect(response.status).to be(200)
      expect(User.find(user.id).verified).to be(true)
    end

    it 'for create user' do
      create :user, :member, verified_at: false

      get :show, params: { id: 'test' }

      expect(response.status).to be(400)
    end
  end
end
