# frozen_string_literal: true

require 'rails_helper'
require 'benchmark'

RSpec.describe Api::V1::FeedbacksController, type: :controller do
  render_views

  let(:member) { create :user, :member }

  describe 'with success' do
    it 'for create by authorized user' do
      sign_in user: member

      post :create, params: {
        email: member.email,
        user_id: member.id,
        message: 'Test',
        full_name: 'User'
      }

      feedback = Feedback.last

      expect(response.status).to be(200)
      expect(Feedback.count).to be(1)
      expect(feedback.message).to eq('Test')
      expect(feedback.user_id).to eq(member.id)
    end

    it 'for create by not registered user' do
      post :create, params: {
        email: 'test@test.com',
        user_id: '',
        message: 'Test',
        full_name: 'User'
      }

      feedback = Feedback.last

      expect(response.status).to be(200)
      expect(Feedback.count).to be(1)
      expect(feedback.message).to eq('Test')
      expect(feedback.user_id).to be_nil
    end
  end
end
