# frozen_string_literal: true

require 'rails_helper'
require 'benchmark'

RSpec.describe Api::V1::UsersController, type: :controller do
  render_views

  describe 'with success' do
    it 'for create member' do
      post :create, params: {
        email: 'member@gmail.com',
        password: 'secret',
        role: 'member'
      }

      expect(response.status).to be(200)
      expect(JSON.parse(response.body)).not_to be_nil
    end

    it 'for update user' do
      user = create :user, :member
      sign_in user: user

      put :update, params: {
        first_name: 'member 1'
      }

      expect(response.status).to be(200)
      expect(JSON.parse(response.body)['first_name']).to eq(User.find(user.id).first_name)
    end

    it 'for user profile' do
      user = create :user, :member
      sign_in user: user

      get :profile
      expect(response.status).to be 200
      expect(JSON.parse(response.body)['email']).to eq(user.email)
    end
  end

  describe 'with error' do
    it 'for create user' do
      User.where(email: 'testuser@gmail.com').destroy_all

      post :create, params: {
        email: '',
        password: 'secret',
        role_id: Role.get_member.id
      }

      expect(response.status).to be(400)
      # expect(JSON.parse(response.body)['errors']['email'][0]).to eq('USER_EMAIL_VALIDATION_FORMAT')
    end

    it 'for validation error for already registered email' do
      user = create :user, :member

      post :create, params: {
        email: user.email,
        password: 'secret',
        role_id: Role.get_member.id
      }

      expect(response.status).to be(400)
      # expect(JSON.parse(response.body)['errors']['email'][0]).to eq('USER_EMAIL_VALIDATION_UNIQ')
    end

    it 'for user profile' do
      create :user, :member
      get :profile
      expect(response.status).to be 400
    end
  end
end
