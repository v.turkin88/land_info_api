# frozen_string_literal: true

require 'rails_helper'
require 'benchmark'

RSpec.describe Api::V1::LocalesController, type: :controller do
  render_views

  let(:member) { create :user, :member }

  describe 'with success' do
    it 'for index' do
      sign_in user: member

      locale = create :locale, active: true

      get :index

      expect(response.status).to be(200)
      expect(JSON.parse(response.body)['locales'].count).to eq(1)
    end

    it 'for index not authorized user' do
      locale = create :locale, active: true

      get :index

      expect(response.status).to be(200)
      expect(JSON.parse(response.body)['locales'].count).to eq(1)
    end
  end
end
