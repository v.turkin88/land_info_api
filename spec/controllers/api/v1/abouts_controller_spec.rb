require 'rails_helper'

RSpec.describe Api::V1::AboutController, type: :controller do
  render_views

  let(:member) { create :user, :member }
  let(:response_body) { JSON.parse(response.body) }
  let(:locale) { create :locale, active: true }

  let(:attributes) do
    {
      body: 'Test body',
      locale_id: locale.id
    }
  end

  describe 'with success' do
    it 'for index when authorized user' do
      sign_in user: member

      About.first_or_create attributes

      get :index, params: { locale: locale.iso3 }
      expect(response.status).to be 200
      expect(JSON.parse(response.body)['about']['body']).to eq('Test body')
    end

    it 'for index when not authorized user' do
      About.first_or_create attributes

      get :index, params: { locale: locale.iso3 }
      expect(response.status).to be 200
      expect(JSON.parse(response.body)['about']['body']).to eq('Test body')
    end
  end
end
