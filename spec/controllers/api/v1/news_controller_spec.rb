require 'rails_helper'
require 'benchmark'

RSpec.describe Api::V1::NewsController, type: :controller do
  render_views

  let(:member) { create :user, :member }
  let(:manager) { create :user, :manager }
  let(:news_params) do
    {
      title: 'test',
      description: 'description test',
      status: 'draft',
      user_id: member.id
    }
  end

  describe 'with success' do
    it 'for index' do
      News.create news_params

      sign_in user: member

      get :index, params: { title: 'test' }

      expect(response.status).to be(200)
      expect(JSON.parse(response.body)['news'][0]['title']).to eq('test')
    end

    it 'for index not authorized user' do
      News.create news_params

      get :index

      expect(response.status).to be(200)
      expect(JSON.parse(response.body)['news'][0]['title']).to eq('test')
    end

    it 'for create news' do
      sign_in user: manager

      post :create, params: news_params

      expect(response.status).to be 200
    end

    it 'for destroy' do
      news = News.create news_params.merge(user_id: manager.id)
      sign_in user: manager

      delete :destroy, params: { id: news.id }

      expect(response.status).to be 200
    end

    it 'for update' do
      sign_in user: manager
      news = News.create news_params

      put :update, params: { id: news.id, title: 'ttt' }

      expect(response.status).to be 200
      expect(News.find_by(id: news.id).title).to eq('ttt')
    end
  end

  describe 'with error' do
    it 'for create news' do
      sign_in user: member

      post :create, params: news_params

      expect(response.status).to be 403
    end

    it 'for destroy by member' do
      news = News.create news_params

      sign_in user: member

      delete :destroy, params: { id: news.id }

      expect(response.status).to be 403
    end

    it 'for update by member' do
      sign_in user: member
  
      news = News.create news_params

      put :update, params: { id: news.id, title: 'ttt' }

      expect(response.status).to be 403
    end
  end
end
