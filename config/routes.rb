Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  # root to: "pages#index"

  match 'api/*all' => 'api/base#cors_preflight_check', :constraints => { method: 'OPTIONS' }, :via => [:options]

  namespace :api do
    namespace :v1 do
      get :verification, to: 'users#verification'

      post :login, to: 'sessions#create'
      post :signup, to: 'users#create'
      delete :logout, to: 'sessions#destroy'
      post :push_token, to: 'sessions#update'

      get :profile, to: 'users#profile'
      put :update_profile, to: 'users#update'
      post :update_forgot_password, to: 'password_resets#update_forgot_password'

      resources :users, only: [:index, :show] do
        collection do
          get :confirm_email
          put :change_password
          get :verification_codes
        end
      end

      resources :password_resets, only: %i[create]
      resources :faqs, only: [:index]
      resources :locales, only: [:index]
      resources :feedbacks, only: [:create]
      resources :questions, only: [:create, :index, :show, :update, :destroy]
      resources :answers, only: [:create, :index, :show, :update, :destroy]
      resources :news, only: [:index, :show, :destroy, :update, :create]

      resources :about, only: [:index]
      resources :privacy_policies, only: [:index]
      resources :verifications, only: %i[show]
    end
  end

  resources :sessions, only: [:create] do
    collection do
      delete :destroy
      get :check
    end
  end
end
