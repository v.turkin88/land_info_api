class News < ApplicationRecord

  belongs_to :user

  validates :title, presence: { message: I18n.t("validations.cant_be_blank") }
  validates :description, presence: { message: I18n.t("validations.cant_be_blank") }

  enum status: {
    draft: 0,
    reviews: 1,
    published: 2
  }

  class << self
    def search_query(params)
      news = News.arel_table

      q = news.project(params[:count] ? "COUNT(*)" : Arel.star)

      if params[:count]
      else
        if News.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
          q.order(news[params[:sort_column]].send(params[:sort_type] == "asc" ? :asc : :desc))
        else
          q.group(news[:id])
          q.order(news[:id].desc)
        end
      end

      q.where(news[:title].matches("%#{params[:title]}%")) if params[:title].present?
      q.where(news[:status].eq(params[:status])) if params[:status].present?
      q.where(news[:description].matches("%#{params[:description]}%")) if params[:description].present?
      q.where(news[:user_id].eq(params[:user_id])) if params[:user_id].present?
      q
    end
  end
end