class Question < ApplicationRecord

  belongs_to :user, required: false

  validates :title, presence: { message: I18n.t("validations.cant_be_blank") }
  validates :description, presence: { message: I18n.t("validations.cant_be_blank") }

  enum status: {
    draft: 0,
    published: 1,
    reviewed: 2
  }

  def self.search_query(params)
    questions = Question.arel_table

    q = questions.project(params[:count] ? "COUNT(*)" : Arel.star)

    if params[:count]
    else
      if Question.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
        q.order(questions[params[:sort_column]].send(params[:sort_type] == "asc" ? :asc : :desc))
      else
        q.group(questions[:id])
        q.order(questions[:id].desc)
      end
    end

    q.where(questions[:title].matches("%#{params[:title]}%")) if params[:title].present?
    q.where(questions[:description].matches("%#{params[:description]}%")) if params[:description].present?
    q.where(questions[:status].eq(params[:status])) if params[:status].present?

    q
  end
end
