class User < ApplicationRecord
  include ApplicationHelper

  has_many :sessions, dependent: :destroy
  has_many :news
  belongs_to :role
  has_many :answers

  attr_accessor :password

  validates :password, presence: true, length: { within: 6..40 }, if: :validate_password?

  validates :email,
            uniqueness: { case_sensitive: false, message: 'USER_EMAIL_VALIDATION_UNIQ' },
            format: { with: /.*@.*\..*/, message: 'USER_EMAIL_VALIDATION_FORMAT' }

  before_validation :downcase_email
  before_save       :encrypt_password
  # before_destroy    :validate_destroy
  before_create     :generate_verification_code
  # after_create :send_welcome_email

  has_attached_file :avatar,
                    styles: { medium: '1000x1000>', thumb: '120x250>' },
                    default_url: '/images/missing.png',
                    path: ':rails_root/public/system/users/:id/avatars/:style/:filename',
                    url: '/system/users/:id/avatars/:style/:filename'

  validates_attachment_size :avatar, less_than: 20.megabytes, unless: proc { |model| model.avatar }
  validates_attachment_content_type :avatar, content_type: %r{\Aimage/.*\Z}

  scope :by_name, ->(q) { where('email ILIKE ?', "%#{q}%", "%#{q}%") }

  Role::NAMES.each do |name_constant|
    define_method("#{name_constant}?") { role.try(:name) == name_constant.to_s }
  end

  class << self
    def query(params)
      users = User.arel_table
      roles = Role.arel_table

      params[:role_ids] ||= []
      params[:roles] ||= []
      params[:roles].each do |role_name|
        params[:role_ids] << Role.send("get_#{role_name}").id if Role.respond_to?("get_#{role_name}")
      end

      q = users
        .project(
              'users.*',
              roles[:name].as('role_name'),
            )
        .group(users[:id], roles[:id])

      q.join(roles, Arel::Nodes::OuterJoin).on(users[:role_id].eq(roles[:id]))
      q.where(users[:id].eq(params[:id])) if params[:id].present?
      q.where(users[:email].matches("%#{params[:email]}%")) if params[:email].present?
      q.where(users[:first_name].matches("%#{params[:first_name]}%")) if params[:first_name].present?
      q.where(users[:last_name].matches("%#{params[:last_name]}%")) if params[:last_name].present?
      q.where(roles[:id].in(params[:role_ids])) if params[:role_ids].present?
      q.where(roles[:name].eq('admin')) if params[:show_admin].blank?

      q
    end

    def search_query(params)
      users = User.arel_table

      q = query(params).as('t')

      result = users.project(params[:count] ? 'COUNT(*)' : 't.*').from(q)

      if params[:count]

      else
        result.order(q[:id].desc)
      end

      result
    end
  end

  def authenticate(password)
    encrypted_password == encrypt(password)
  end

  def validate_destroy
    if admin? && User.where(role_id: Role.get_admin.id).count == 1
      errors.add :base, 'Can not remove last admin.'
      throw :abort
      false
    elsif User.count == 1

      errors.add :base, 'Can not remove last user.'
      throw :abort
      false
    end
  end

  def to_json_login
    {
      id: id.to_s,
      email: email,
      first_name: first_name,
      last_name: last_name,
    }
  end

  def to_sign_up_json
    {
      id: id.to_s,
      email: email,
      first_name: first_name,
      last_name: last_name,
      role_id: role.id,
      role_name: role.name,
    }
  end

  def send_password_reset
    # update_attribute :reset_password_token, encrypt(self.email + Time.now.to_s)
    update_attribute :reset_password_token, digital_code(12)
    UserMailer.password_reset(self).deliver_now
  rescue Exception => e
    puts e.message
  end

  def password_changed
    UserMailer.password_changed(self).deliver_now
  rescue Exception => e
    puts e.message
  end

  def find_user(attribute)
    user = User.where('email = ?', attribute)
    user.first
  end

  def completed_verification
    UserMailer.verification_email(self).deliver_now
  rescue Exception => e
    puts e.message
  end

  def verified_user
    update(verification_code: nil, verified_at: Time.now, verified: true)
    # completed_verification
  rescue Exception => e
    puts e.message
  end

  def verified?
    self.verified
  end

  private

  def validate_password?
    admin? && (new_record? || !password.nil?)
  end

  def downcase_email
    self.email = email.downcase if email
  end

  def encrypt_password
    self.salt = make_salt if salt.blank?
    self.encrypted_password = encrypt(password) if password
  end

  def generate_verification_code
    self.verification_code = digital_code(10)
  end

  def encrypt(string)
    secure_hash("#{string}--#{salt}")
  end

  def make_salt
    secure_hash("#{Time.now.utc}--#{password}")
  end

  def secure_hash(string)
    Digest::SHA2.hexdigest(string)
  end

  def digital_code(data)
    data.times.map { rand(10) }.join
  end

  def send_welcome_email
    # UserMailer.welcome_email(self).deliver_now if self.email.present?
  rescue Exception => e
    puts e.message
  end
end
