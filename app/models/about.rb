# frozen_string_literal: true

class About < ApplicationRecord
  belongs_to :locale

  validates :body, presence: { message: I18n.t('validations.cant_be_blank') }
  validates :locale_id, presence: { message: I18n.t('validations.cant_be_blank') }

  def self.search_query(params)
    abouts = About.arel_table

    q = abouts.project(params[:count] ? 'COUNT(*)' : Arel.star)

    if params[:count]
    elsif About.column_names.include?(params[:sort_column]) && %w[asc desc].include?(params[:sort_type])
      q.order(abouts[params[:sort_column]].send(params[:sort_type] == 'asc' ? :asc : :desc))
    else
      q.group(abouts[:id])
      q.order(abouts[:id].desc)
    end

    # q.where(abouts[:title].matches("%#{params[:titleey]}%")) if params[:title].present?

    q
  end
end
