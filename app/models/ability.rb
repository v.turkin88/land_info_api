# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new

    if user.admin?
      can :manage, :all
    elsif user.member?
      member_abilities(user)
    elsif user.manager?
      manager_abilities(user)
    else
      # can :index, Role
      can :index, Locale
      can :manage, Answer
      # can :index, About
      # can :index, Faq
      # can :create, Feedback
      # can :index, Subject
      # can :index, Ganre
      # can :index, PrivacyPolicy
      # can :manage, GeneralLocation
      can :manage, News
    end
  end

  def member_abilities(_user)
    can :index, User
    can :show, User
    can :index, News

    can :index, Faq
    can :index, Locale
    can :create, Feedback
    can :manage, Question
    can :manage, Answer
  end

  def manager_abilities(_user)
    can :index, Faq
    can :index, Locale
    can :create, Feedback
    can :manage, Answer
    can :manage, News
    can :index, User
    can :show, User
  end
end
