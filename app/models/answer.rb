class Answer < ApplicationRecord

  # belongs_to :question
  belongs_to :user

  validates :description, presence: { message: I18n.t("validations.cant_be_blank") }

  def self.search_query(params)
    answers = Answer.arel_table

    q = answers.project(params[:count] ? "COUNT(*)" : Arel.star)

    if params[:count]
    else
      if Answer.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
        q.order(answers[params[:sort_column]].send(params[:sort_type] == "asc" ? :asc : :desc))
      else
        q.group(answers[:id])
        q.order(answers[:id].desc)
      end
    end

    q.where(answers[:description].matches("%#{params[:description]}%")) if params[:description].present?
    # q.where(questions[:status].eq(params[:status])) if params[:status].present?
    # q.where(answers[:question_id].eq(params[:question_id])) if params[:question_id].present?
    # q.where(answers[:user_id].eq(params[:user_id])) if params[:user_id].present?

    q
  end
end