class Faq < ApplicationRecord

  belongs_to :locale

  validates :title, presence: { message: I18n.t("validations.cant_be_blank") }
  validates :description, presence: { message: I18n.t("validations.cant_be_blank") }
  validates :locale_id, presence: { message: I18n.t("validations.cant_be_blank") }

  def self.search_query(params)
    faqs = Faq.arel_table

    q = faqs.project(params[:count] ? "COUNT(*)" : Arel.star)

    if params[:count]
    else
      if Faq.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
        q.order(faqs[params[:sort_column]].send(params[:sort_type] == "asc" ? :asc : :desc))
      else
        q.group(faqs[:id])
        q.order(faqs[:id].desc)
      end
    end

    q.where(faqs[:title].matches("%#{params[:title]}%")) if params[:title].present?
    q.where(faqs[:description].matches("%#{params[:description]}%")) if params[:description].present?
    q.where(faqs[:locale_id].eq(params[:locale_id])) if params[:locale_id].present?

    q
  end
end
