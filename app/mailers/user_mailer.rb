class UserMailer < ApplicationMailer

  def welcome_email(user)
    @user = user
    @logo = 'https://www.google.com/url?sa=i&url=http%3A%2F%2Fwww.velizaratellalyan.com%2Fseo%2Fwhats-the-difference-between-dynamic-and-static-urls%2F&psig=AOvVaw2hOE_d7gaOQru8dr5kADwU&ust=1613223954642000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCPCgtti95O4CFQAAAAAdAAAAABAD'
    @url = "https://landcase.com/api/v1/verification?code=#{@user.verification_code}"
    mail(to: @user.email, subject: 'Welcome to LandCase')
  end

  def verification_email(user)
    @user = user
    @logo = 'https://www.google.com/url?sa=i&url=http%3A%2F%2Fwww.velizaratellalyan.com%2Fseo%2Fwhats-the-difference-between-dynamic-and-static-urls%2F&psig=AOvVaw2hOE_d7gaOQru8dr5kADwU&ust=1613223954642000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCPCgtti95O4CFQAAAAAdAAAAABAD'
    mail(to: @user.email, subject: 'LandCase. Verification completed.')
  end

  def password_reset(user)
    @user = user
    @logo = 'https://www.google.com/url?sa=i&url=http%3A%2F%2Fwww.velizaratellalyan.com%2Fseo%2Fwhats-the-difference-between-dynamic-and-static-urls%2F&psig=AOvVaw2hOE_d7gaOQru8dr5kADwU&ust=1613223954642000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCPCgtti95O4CFQAAAAAdAAAAABAD'
    @url = "http://landcase.com/users/password_reset?t=#{ @user.reset_password_token }"

    mail(to: @user.email, subject: 'LandCase. Reset password instructions.')
  end

  def password_changed(user)
    @user = user
    @logo = 'https://www.google.com/url?sa=i&url=http%3A%2F%2Fwww.velizaratellalyan.com%2Fseo%2Fwhats-the-difference-between-dynamic-and-static-urls%2F&psig=AOvVaw2hOE_d7gaOQru8dr5kADwU&ust=1613223954642000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCPCgtti95O4CFQAAAAAdAAAAABAD'

    mail(to: @user.email, subject: 'LandCase. Password changed.')
  end
end
