class ApplicationMailer < ActionMailer::Base
  default from: 'admin@landcase.com'
  layout 'mailer'
end
