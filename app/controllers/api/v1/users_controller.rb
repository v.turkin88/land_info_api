class Api::V1::UsersController < Api::V1::BaseController
  load_and_authorize_resource :user, only: [:show]
  skip_before_action :authenticate_user, only: [:create, :verification ]

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 6 if per_page < 1

    @users = User.search_query search_params
    @count = User.search_query search_params.merge count: true
  end

  def create
    role_id = Role.find_by(name: params[:role]).id
    @existing_user = User.find_by(email: params[:email], role_id: role_id)

    if @existing_user && !@existing_user.verified?
      @existing_user.destroy
      @user = User.new create_params
    else
      @user = User.new create_params
    end

    if @user.save
      render json: { status: 'success', verification_code: @user.verification_code }
    else
      render json: { errors: @user.errors }, status: :bad_request
    end
  end

  def verification
    user = User.find_by(verification_code: params[:code])

    render json: { errors: ['Code not found'] }, status: 404 and return unless user.present?

    if user.update_attributes(verified: true, verification_code: '')
      user.completed_verification
      render json: { status: 'success' }
    else
      render json: { errors: user.errors.full_messages }, status: 422
    end

  end

  def verification_codes
    @verification_code = current_user.verification_code
  end

  def show
    @user = User.find params[:id]
    authorize! :show, @user
  end

  def profile
    @user = current_user
  end

  def change_password
    if params[:new_password].present? && params[:password_confirmation].present? && params[:new_password].present? == params[:password_confirmation].present?
      user = current_user
      if user.authenticate(params[:current_password])
        if user.update_attributes password: params[:new_password]
          render json: { message: 'Password updated.' }
        else
          render json: { errors: user.errors.full_messages }, status: :unprocessable_entity
        end
      else
        render json: { errors: 'Wrong Password' }, status: :unprocessable_entity
      end
    else
      render json: { message: 'new_password and password_confirmation not availble or not equals' },
             status: :unprocessable_entity
    end
  end

  def update
    @user = current_user

    if @user.update update_params
      render json: @user.to_sign_up_json
    else
      render json: { errors: @user.errors.map { |_k, v| v } }, status: :bad_request
    end
  end

  def update_avatar_or_cover
    @user = current_user

    if @user.update_attributes update_avatar_params
      render json: @user.to_sign_up_json
    else
      render json: {errors: @user.errors.map{|k, v| v}}, status: :bad_request
    end
  end


  private

  def user_params
    allowed_params = params.permit :email, :first_name, :last_name, :role_id, :avatar
    allowed_params[:current_user] = current_user
    allowed_params
  end

  def update_params
    allowed_params = params.permit :first_name, :last_name, :avatar
    allowed_params[:avatar] = nil if allowed_params[:avatar] == 'null'
    allowed_params
  end

  def create_params
    allowed_params = params.permit :email, :password
    allowed_params[:role_id] = Role.find_by(name: params[:role]).id

    allowed_params
  end
end
