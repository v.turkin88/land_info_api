class Api::V1::LocalesController < Api::V1::BaseController

  skip_before_action :authenticate_user, only: [:index]

  def index
    @locales = Locale.where(active: true)
  end

end
