class Api::V1::NewsController < Api::V1::BaseController

  skip_before_action :authenticate_user
  load_and_authorize_resource :news

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 6 if per_page < 1

    query = News.search_query params
    count_query = News.search_query params.merge(count: true)

    @news = News.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = News.find_by_sql(count_query.to_sql).first.try(:[], "count").to_i
  end

  def show
    @id = params[:id]
  end

  def create
    @news = News.new create_params

    if @news.save
      render json: { message: I18n.t('messages.success_upsert') }
    else
      render json: { validation_errors: @news.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    @news.destroy
    render json: { message: I18n.t('messages.success_upsert') }
  end

  def update
    if @news.update news_params
      render json: { message: I18n.t('messages.success_upsert') }
    else
      render json: { validation_errors: @news.errors }, status: :unprocessable_entity
    end
  end

  private

  def news_params
    allowed_params = params.permit :title, :description, :active
    allowed_params[:user_id] = current_user.id
    allowed_params
  end

  def create_params
    allowed_params = params.permit :title, :description, :active
    allowed_params[:user_id] = current_user.id
    allowed_params
  end
end
