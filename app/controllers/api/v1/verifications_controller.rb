class Api::V1::VerificationsController < Api::V1::BaseController
  skip_before_action :authenticate_user

  def show
    user = User.find_by(verification_code: params[:id])

    if user.present?
      user.verified_user
      render json: { message: 'successfully' }
    else
      render json: { errors: [I18n.t('reset_password.user_not_found')] }, status: :bad_request
    end
  end
end
