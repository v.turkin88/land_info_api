class Api::V1::FaqsController < Api::V1::BaseController
  skip_before_action :authenticate_user, only: [:index, :show]

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    locale = Locale.locale(params[:locale])

    query = Faq.search_query params.merge(locale_id: locale.id)
    count_query = Faq.search_query params.merge(count: true, locale_id: locale.id)

    @faqs = Faq.includes(:locale).find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = Faq.find_by_sql(count_query.to_sql).first.try(:[], "count").to_i
  end
end
