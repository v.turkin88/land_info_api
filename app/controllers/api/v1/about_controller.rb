class Api::V1::AboutController < Api::V1::BaseController
  skip_before_action :authenticate_user

  def index
    locale = Locale.locale(params[:locale].downcase)
    @about = About.find_by(locale_id: locale.id)
  rescue StandardError => e
    render json: { message: 'Locale not found' }, status: :unprocessable_entity and return
  end
end
