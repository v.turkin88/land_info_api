class Api::V1::FeedbacksController < Api::V1::BaseController

  skip_before_action :authenticate_user

  def create
    @feedback = Feedback.new feedback_params

    if @feedback.save
      render json: { message: I18n.t('messages.success_upsert') }
    else
      render json: { validation_errors: @feedback.errors }, status: :unprocessable_entity
    end
  end

  private

  def feedback_params
    allowed_params = params.permit :email, :user_id, :full_name, :message
    allowed_params
  end
end
