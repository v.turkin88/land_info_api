module ApplicationHelper
  def paperclip_url(paperclip_attachment, style = nil)
    return nil unless paperclip_attachment

    if Paperclip::Attachment.default_options[:storage] == :s3
      if paperclip_attachment&.instance["#{paperclip_attachment.name}_file_name"]
        'http:' + paperclip_attachment.try(
      :url, style
    )
      end
    elsif paperclip_attachment&.instance["#{paperclip_attachment.name}_file_name"]
      paperclip_attachment.try(:url,
                               style).to_s
    end
  end
end
