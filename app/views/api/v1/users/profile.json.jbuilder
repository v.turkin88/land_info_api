json.extract! @user, :id, :email, :first_name, :last_name, :verified

json.role do
  json.extract! @user.role, :id, :name
end

# json.avatar                   @user.avatar.url
# json.avatar_preview           @user.avatar.url :medium