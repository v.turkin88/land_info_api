json.news do
  json.extract! @news, :id, :title, :description, :status, :active
end