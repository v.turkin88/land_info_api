json.locales @locales.each do |locale|
  json.extract! locale, :iso2, :iso3, :title
end
json.count @locales.count