json.privacy_policy do
  json.extract! @privacy_policy, :body, :document_url, :locale_id
end
