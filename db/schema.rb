# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_07_06_143914) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "abouts", force: :cascade do |t|
    t.text "body"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "locale_id", null: false
    t.index ["locale_id"], name: "index_abouts_on_locale_id", unique: true
  end

  create_table "answers", force: :cascade do |t|
    t.text "description"
    t.integer "question_id"
    t.integer "user_id"
    t.integer "status"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "content_keys", force: :cascade do |t|
    t.string "key", null: false
  end

  create_table "email_senders", force: :cascade do |t|
    t.string "address"
    t.string "port"
    t.string "domain"
    t.string "authentication"
    t.string "user_name"
    t.string "password"
    t.boolean "enable_starttls_auto"
  end

  create_table "faqs", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.boolean "active"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "locale_id"
  end

  create_table "feedback_responses", force: :cascade do |t|
    t.text "body", null: false
    t.bigint "feedback_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "feedbacks", force: :cascade do |t|
    t.string "email"
    t.string "message"
    t.string "full_name"
    t.integer "user_id"
    t.boolean "viewed", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "locales", force: :cascade do |t|
    t.string "title"
    t.string "iso2"
    t.string "iso3"
    t.boolean "active", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "news", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.integer "status"
    t.boolean "active", default: false
    t.integer "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "privacy_policies", force: :cascade do |t|
    t.text "body"
    t.bigint "locale_id", null: false
    t.string "document_url"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["locale_id"], name: "index_privacy_policies_on_locale_id", unique: true
  end

  create_table "questions", force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.integer "user_id"
    t.integer "status"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "sessions", force: :cascade do |t|
    t.string "token"
    t.bigint "user_id"
    t.string "push_token"
    t.integer "device_type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_sessions_on_user_id"
  end

  create_table "translations", force: :cascade do |t|
    t.string "title"
    t.integer "content_key_id"
    t.integer "locale_id"
    t.boolean "active", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["locale_id", "content_key_id"], name: "index_translations_on_locale_id_and_content_key_id", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "encrypted_password"
    t.string "salt"
    t.string "email"
    t.string "first_name"
    t.string "last_name"
    t.bigint "role_id"
    t.datetime "last_logged_in"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "avatar_file_name"
    t.string "avatar_content_type"
    t.bigint "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.datetime "verified_at"
    t.datetime "reset_password_at"
    t.string "verification_code"
    t.boolean "verified", default: false
    t.index ["role_id"], name: "index_users_on_role_id"
  end

end
