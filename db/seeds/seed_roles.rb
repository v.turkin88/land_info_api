def seed_roles
  Role.destroy_all

  roles_attributes = [
    { name: :member},
    { name: :manager},
    { name: :admin},
  ]

  roles_attributes.each do |role|
    Role.create role
  end
end
