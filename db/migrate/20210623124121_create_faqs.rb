class CreateFaqs < ActiveRecord::Migration[6.1]
  def change
    create_table :faqs do |t|
      t.string :title
      t.text :description
      t.boolean :active

      t.timestamps
    end
  end
end
