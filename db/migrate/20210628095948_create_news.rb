class CreateNews < ActiveRecord::Migration[6.1]
  def change
    create_table :news do |t|
      t.string :title
      t.text :description
      t.integer :status
      t.boolean :active, default: false
      t.integer :user_id

      t.timestamps
    end
  end
end
