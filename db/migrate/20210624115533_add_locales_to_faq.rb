class AddLocalesToFaq < ActiveRecord::Migration[6.1]
  def change
    add_column :faqs, :locale_id, :integer
  end
end
