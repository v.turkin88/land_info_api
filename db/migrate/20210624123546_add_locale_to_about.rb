class AddLocaleToAbout < ActiveRecord::Migration[6.1]
  def change
    add_column :abouts, :locale_id, :bigint, null: false

    add_index :abouts, :locale_id, unique: true
  end
end
