class CreateAbout < ActiveRecord::Migration[6.1]
  def change
    create_table :abouts do |t|
      t.text :body

      t.timestamps
    end
  end
end
