class CreateFeedbacks < ActiveRecord::Migration[6.1]
  def change
    create_table :feedbacks do |t|
      t.string :email, nil: false
      t.string :message, nil: false
      t.string :full_name, nil: false
      t.integer :user_id
      t.boolean :viewed, default: false

      t.timestamps
    end
  end
end
