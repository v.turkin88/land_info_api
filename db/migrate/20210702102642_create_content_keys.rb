class CreateContentKeys < ActiveRecord::Migration[5.1]
  def change
    create_table :content_keys do |t|
      t.string :key, null: false, unique: true
    end
  end
end
