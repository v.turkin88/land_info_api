class CreateQuestions < ActiveRecord::Migration[6.1]
  def change
    create_table :questions do |t|
      t.string :title
      t.string :description
      t.integer :user_id
      t.integer :status

      t.timestamps
    end
  end
end
